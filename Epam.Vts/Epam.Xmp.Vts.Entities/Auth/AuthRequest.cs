﻿namespace Epam.Xmp.Vts.Entities.Auth
{
    public class AuthRequest
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}