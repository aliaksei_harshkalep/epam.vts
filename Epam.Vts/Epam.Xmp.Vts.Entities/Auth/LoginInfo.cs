﻿namespace Epam.Xmp.Vts.Entities.Auth
{
    public class LoginInfo
    {
        public string PersonId { get; set; }
        public string Password { get; set; }
    }
}
