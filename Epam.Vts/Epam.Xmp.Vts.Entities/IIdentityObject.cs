﻿namespace Epam.Xmp.Vts.Entities
{
    public interface IIdentityObject<T>
    {
        T Id { get; }
    }
}