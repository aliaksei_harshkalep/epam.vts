using SQLite.Net;
using SQLite.Net.Attributes;

namespace Epam.Xmp.Vts.Entities
{
    public class Person : IIdentityObject<string>
    {
        public string Email { get; set; }

        [PrimaryKey]
        public string Id { get; set; }

        public string FullName { get; set; }

        public string Region { get; set; }

        [Ignore]
        public string[] ApproversId { get; set; }
    }
}