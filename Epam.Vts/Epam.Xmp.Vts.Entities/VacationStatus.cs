﻿using SQLite.Net.Attributes;

namespace Epam.Xmp.Vts.Entities
{
    public class VacationStatus : IIdentityObject<string>
    {
        [PrimaryKey]
        public string Id { get; set; }

        public string Value { get; set; }

        public string Icon { get; set; }
    }
}