﻿using SQLite.Net.Attributes;

namespace Epam.Xmp.Vts.Entities
{
    public class VacationInfo : IIdentityObject<int>
    {
        public long Duration { get; set; }

        public string DurationStr { get; set; }

        public long EndDate { get; set; }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public long StartDate { get; set; }

        [Indexed]
        public string ApproverId { get; set; }

        [Indexed]
        public string EmployeeId { get; set; }

        [Indexed]
        public string StatusId { get; set; }

        [Indexed]
        public string TypeId { get; set; }
    }
}