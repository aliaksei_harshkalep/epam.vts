﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories
{
    public interface IVacationTypeRepository
    {
        Task<IEnumerable<VacationType>> GetAllAsync();
        Task<VacationType> GetByIdAsync(string id);
    }
}