﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories
{
    public interface IVacationStatusRepository
    {
        Task<IEnumerable<VacationStatus>> GetAllAsync();
        Task<VacationStatus> GetByIdAsync(string id);
    }
}