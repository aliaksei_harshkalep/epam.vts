﻿using Epam.Vts.Core.Helpers;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.ServiceBased
{
    public class ServicePersonRepository : BaseServiceRepository<string, Person>, IPersonRepository
    {
        public override string ServiceUrl => Settings.PersonUrl;
    }
}