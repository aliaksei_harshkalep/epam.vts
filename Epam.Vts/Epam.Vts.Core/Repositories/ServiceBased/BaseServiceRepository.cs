﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Epam.Vts.Core.Helpers;
using Newtonsoft.Json;

namespace Epam.Vts.Core.Repositories.ServiceBased
{
    public abstract class BaseServiceRepository<TKey, TEntity>
    {
        public abstract string ServiceUrl { get; }

        public IEnumerable<TEntity> GetAll()
        {
            using (var client = new HttpClient())
            {
                var result = client.GetAsync(ServiceUrl).Result;
                var jsonData = result.Content.ReadAsStringAsync().Result;
                var entities = JsonConvert.DeserializeObject<IEnumerable<TEntity>>(jsonData);

                return entities;
            }
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(ServiceUrl);
                var jsonData = await result.Content.ReadAsStringAsync();
                var entities = JsonConvert.DeserializeObject<IEnumerable<TEntity>>(jsonData);

                return entities;
            }
        }

        public async Task<TEntity> GetByIdAsync(TKey id)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(ServiceUrl + "/" + id);
                var entity = JsonConvert.DeserializeObject<TEntity>(await result.Content.ReadAsStringAsync());
                return entity;
            }
        }

        public async Task AddOrUpdateAsync(TEntity entity)
        {
            using (var client = new HttpClient())
            {
                var data = JsonConvert.SerializeObject(entity);
                await client.PutAsync(ServiceUrl, new StringContent(data, Encoding.UTF8, "application/json"));
            }
        }

        public async Task DeleteAsync(TKey id)
        {
            using (var client = new HttpClient())
            {
                await client.DeleteAsync(ServiceUrl + "/" + id);
            }
        }
    }
}