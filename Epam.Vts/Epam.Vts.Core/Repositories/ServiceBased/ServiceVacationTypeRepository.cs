﻿using Epam.Vts.Core.Helpers;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.ServiceBased
{
    public class ServiceVacationTypeRepository : BaseServiceRepository<string, VacationType>, IVacationTypeRepository
    {
        public override string ServiceUrl => Settings.VacationTypeUrl;
    }
}