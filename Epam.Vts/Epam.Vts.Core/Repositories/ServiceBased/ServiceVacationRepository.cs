﻿using Epam.Vts.Core.Helpers;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.ServiceBased
{
    public class ServiceVacationRepository : BaseServiceRepository<int, VacationInfo>, IVacationRepository
    {
        public override string ServiceUrl => Settings.VacationsUrl;
    }
}