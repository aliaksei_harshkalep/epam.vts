﻿using Epam.Vts.Core.Helpers;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.ServiceBased
{
    public class ServiceVacationStatusRepository : BaseServiceRepository<string, VacationStatus>, IVacationStatusRepository
    {
        public override string ServiceUrl => Settings.VacationStatusUrl;
    }
}