using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories
{
    public class MockVacationRepository : IVacationRepository
    {
        //private static readonly List<VacationInfo> Vacations =
        //    new List<VacationInfo>
        //    {
        //        new VacationInfo
        //        {
        //            Id = 1,
        //            Approver = new Person
        //            {
        //                Id = "1",
        //                Email = "approver@epam.com",
        //                FullName = "Approver",
        //                Region = "Belarus"
        //            },
        //            Duration = new TimeSpan(2, 0, 0, 0),
        //            DurationStr = "10",
        //            Employee = new Person
        //            {
        //                Email = "person@epam.com",
        //                FullName = "Person",
        //                Region = "Belarus",
        //                Id = "2"
        //            },
        //            EndDate = new DateTime(2015, 10, 20),
        //            StartDate = new DateTime(2015, 10, 21),
        //            ProcessInstanceId = "1",
        //            Status = new IconedValue(),
        //            Type = new IconedValue(),
        //            VacationForm = null
        //        },
        //        new VacationInfo
        //        {
        //            Id = 1,
        //            Approver = new Person
        //            {
        //                Id = "1",
        //                Email = "approver@epam.com",
        //                FullName = "Approver",
        //                Region = "Belarus"
        //            },
        //            ConfirmationDocumentAvailable = false,
        //            Duration = new TimeSpan(2, 0, 0, 0),
        //            DurationStr = "10",
        //            Employee = new Person
        //            {
        //                Email = "person@epam.com",
        //                FullName = "Person",
        //                Region = "Belarus",
        //                Id = "2"
        //            },
        //            EndDate = new DateTime(2015, 10, 20),
        //            StartDate = new DateTime(2015, 10, 21),
        //            ProcessInstanceId = "1",
        //            Status = new IconedValue(),
        //            Type = new IconedValue(),
        //            VacationForm = null
        //        },
        //    };

        public Task<IEnumerable<VacationInfo>> GetAllAsync()
        {
            throw new NotImplementedException();
            //return Task.FromResult<IEnumerable<VacationInfo>>(Vacations);
        }

        public Task<VacationInfo> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
            //return Task.FromResult(Vacations.FirstOrDefault(v => v.Id == id));
        }

        public Task AddOrUpdateAsync(VacationInfo vacation)
        {
            throw new NotImplementedException();
            //Vacations.Add(vacation);

            //return Task.FromResult(true);
        }

        public Task DeleteAsync(int id)
        {
            throw new NotImplementedException();
            //Vacations.RemoveAll(v => v.Id == id);

            //return Task.FromResult(true);
        }
    }
}