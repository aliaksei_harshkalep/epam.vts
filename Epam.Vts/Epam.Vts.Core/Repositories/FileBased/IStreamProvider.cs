﻿using System.IO;

namespace Epam.Vts.Core.Repositories.FileBased
{
    public interface IStreamProvider
    {
        StreamReader GetReader(string path);
        StreamWriter GetWriter(string path);
    }
}