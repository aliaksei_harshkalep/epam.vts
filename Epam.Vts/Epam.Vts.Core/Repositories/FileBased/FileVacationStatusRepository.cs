using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.FileBased
{
    public class FileVacationStatusRepository : BaseFileRepository<string, VacationStatus>, IVacationStatusRepository
    {
        public FileVacationStatusRepository(IStreamProvider streamProvider) : base(streamProvider)
        {
        }
    }
}