using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.FileBased
{
    public class FileVacationTypeRepository : BaseFileRepository<string, VacationType>, IVacationTypeRepository
    {
        public FileVacationTypeRepository(IStreamProvider streamProvider) : base(streamProvider)
        {
        }
    }
}