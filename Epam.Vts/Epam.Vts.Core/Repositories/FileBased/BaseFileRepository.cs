﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;
using Newtonsoft.Json;

namespace Epam.Vts.Core.Repositories.FileBased
{
    public class BaseFileRepository<TKey, TEntity> where TEntity : IIdentityObject<TKey>
    {
        private readonly string _path = typeof(TEntity).Name + ".json";
        private readonly IStreamProvider _streamProvider;

        public BaseFileRepository(IStreamProvider streamProvider)
        {
            _streamProvider = streamProvider;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            using (var sr = _streamProvider.GetReader(_path))
            {
                var data = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TEntity>>(data);
            }
        }

        public async Task<TEntity> GetByIdAsync(TKey id)
        {
            using (var sr = _streamProvider.GetReader(_path))
            {
                var data = await sr.ReadToEndAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TEntity>>(data).FirstOrDefault(e => e.Id.Equals(id));
            }
        }

        public async Task AddOrUpdateAsync(TEntity entity)
        {
            var entities = (await GetAllAsync()).ToList();

            entities.RemoveAll(e => e.Id.Equals(entity.Id));
            entities.Add(entity);

            using (var sw = _streamProvider.GetWriter(_path))
            {
                var data = JsonConvert.SerializeObject(entities);
                await sw.WriteAsync(data);
            }
        }

        public void AddOrUpdate(IEnumerable<TEntity> entities)
        {
            using (var sw = _streamProvider.GetWriter(_path))
            {
                var data = JsonConvert.SerializeObject(entities);
                sw.Write(data);
            }
        }

        public async Task DeleteAsync(TKey id)
        {
            var entities = (await GetAllAsync()).ToList();

            entities.RemoveAll(e => e.Id.Equals(id));

            using (var sw = _streamProvider.GetWriter(_path))
            {
                var data = JsonConvert.SerializeObject(entities);
                await sw.WriteAsync(data);
            }
        }
    }
}