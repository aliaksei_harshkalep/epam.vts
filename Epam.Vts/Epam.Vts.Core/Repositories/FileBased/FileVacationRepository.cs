using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.FileBased
{
    public class FileVacationRepository : BaseFileRepository<int, VacationInfo>, IVacationRepository
    {
        public FileVacationRepository(IStreamProvider streamProvider) : base(streamProvider)
        {
        }
    }
}