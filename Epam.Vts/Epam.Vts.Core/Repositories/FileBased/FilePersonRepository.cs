using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories.FileBased
{
    public class FilePersonRepository : BaseFileRepository<string, Person>, IPersonRepository
    {
        public FilePersonRepository(IStreamProvider streamProvider) : base(streamProvider)
        {
        }
    }
}