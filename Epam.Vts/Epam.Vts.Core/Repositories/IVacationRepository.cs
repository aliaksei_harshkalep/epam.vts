﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Repositories
{
    public interface IVacationRepository
    {
        Task<IEnumerable<VacationInfo>> GetAllAsync();
        Task<VacationInfo> GetByIdAsync(int id);
        Task AddOrUpdateAsync(VacationInfo vacation);
        Task DeleteAsync(int id);
    }
}