﻿using Epam.Xmp.Vts.Entities;
using SQLite.Net.Async;

namespace Epam.Vts.Core.Repositories.SqliteBased
{
    public class SqliteVacationTypeRepository : BaseSqliteRepository<string, VacationType>, IVacationTypeRepository
    {
        public SqliteVacationTypeRepository(SQLiteAsyncConnection connection) : base(connection)
        {
        }
    }
}