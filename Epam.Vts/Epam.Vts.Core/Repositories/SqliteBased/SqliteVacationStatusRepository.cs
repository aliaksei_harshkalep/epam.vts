﻿using Epam.Xmp.Vts.Entities;
using SQLite.Net.Async;

namespace Epam.Vts.Core.Repositories.SqliteBased
{
    public class SqliteVacationStatusRepository : BaseSqliteRepository<string, VacationStatus>, IVacationStatusRepository
    {
        public SqliteVacationStatusRepository(SQLiteAsyncConnection connection) : base(connection)
        {
        }
    }
}