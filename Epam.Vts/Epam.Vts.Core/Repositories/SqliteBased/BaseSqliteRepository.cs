﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Interop;

namespace Epam.Vts.Core.Repositories.SqliteBased
{
    public class BaseSqliteRepository<TKey, TEntity> where TEntity : class
    {
        private readonly SQLiteAsyncConnection _connection;

        public BaseSqliteRepository(SQLiteAsyncConnection connection)
        {
            _connection = connection;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync() => await _connection.Table<TEntity>().ToListAsync();

        public async Task<TEntity> GetByIdAsync(TKey id) => await _connection.GetAsync<TEntity>(id);

        public async Task AddOrUpdateAsync(TEntity entity) => await _connection.InsertAsync(entity);

        public async Task DeleteAsync(TKey id) => await _connection.DeleteAsync<TEntity>(id);
    }
}