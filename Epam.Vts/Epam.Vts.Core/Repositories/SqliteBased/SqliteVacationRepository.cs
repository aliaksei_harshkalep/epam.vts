﻿using Epam.Xmp.Vts.Entities;
using SQLite.Net.Async;

namespace Epam.Vts.Core.Repositories.SqliteBased
{
    public class SqliteVacationRepository: BaseSqliteRepository<int, VacationInfo>, IVacationRepository
    {
        public SqliteVacationRepository(SQLiteAsyncConnection connection) : base(connection)
        {
        }
    }
}