using Epam.Xmp.Vts.Entities;
using SQLite.Net.Async;
using SQLite.Net.Interop;

namespace Epam.Vts.Core.Repositories.SqliteBased
{
    public class SqlitePersonRepository : BaseSqliteRepository<string, Person>, IPersonRepository
    {
        public SqlitePersonRepository(SQLiteAsyncConnection connection) : base(connection)
        {
        }
    }
}