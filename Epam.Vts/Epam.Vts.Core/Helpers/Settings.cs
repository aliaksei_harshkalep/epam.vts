// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Epam.Vts.Core.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private const string AuthenticationUrlKey = "AuthenticationUrl";
        private static readonly string SettingsDefault = string.Empty;

        #endregion


        public static string GeneralSettings
        {
            get { return AppSettings.GetValueOrDefault<string>(SettingsKey, SettingsDefault); }
            set { AppSettings.AddOrUpdateValue<string>(SettingsKey, value); }
        }

        public static string LoginUrl => "http://10.9.213.5:9884/api/Login/";
        public static string VacationsUrl => "http://10.9.213.5:9884/api/Vacations/";
        public static string VacationTypeUrl => "http://10.9.213.5:9884/api/VacationType/";
        public static string PersonUrl => "http://10.9.213.5:9884/api/Person/";
        public static string VacationStatusUrl => "http://10.9.213.5:9884/api/VacationStatus/";
    }
}