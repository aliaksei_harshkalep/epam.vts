﻿using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Services
{
    public interface IAuthenticationService
    {
        Task<Person> AuthenticateAsync(string userName, string password);
    }
}