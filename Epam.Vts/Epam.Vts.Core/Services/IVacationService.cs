﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Services
{
    public interface IVacationService
    {
        Task<IEnumerable<VacationInfo>> GetEmployeeVacationsAsync(Person employee);
    }
}