﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Epam.Vts.Core.Helpers;
using Epam.Xmp.Vts.Entities;
using Newtonsoft.Json;

namespace Epam.Vts.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        public Person Authenticate(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException(nameof(userName));
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException(nameof(password));

            using (var client = new HttpClient())
            {
                var data = JsonConvert.SerializeObject(new { Name = userName, Password = password });

                var result = client.PostAsync(Settings.LoginUrl, new StringContent(data, Encoding.UTF8, "application/json")).Result;
                var person = JsonConvert.DeserializeObject<Person>(result.Content.ReadAsStringAsync().Result);

                return person;
            }
        }

        public async Task<Person> AuthenticateAsync(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException(nameof(userName));
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException(nameof(password));

            using (var client = new HttpClient())
            {
                var data = JsonConvert.SerializeObject(new {Name = userName, Password = password});

                var result = await client.PostAsync(Settings.LoginUrl, new StringContent(data, Encoding.UTF8, "application/json"));
                var person = JsonConvert.DeserializeObject<Person>(await result.Content.ReadAsStringAsync());

                return person;
            }
        }
    }
}