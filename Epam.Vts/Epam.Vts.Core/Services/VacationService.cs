﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Epam.Vts.Core.Repositories;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Services
{
    public class VacationService : IVacationService
    {
        private readonly IVacationRepository _vacationRepository;

        public VacationService(IVacationRepository vacationRepository)
        {
            _vacationRepository = vacationRepository;
        }

        public async Task<IEnumerable<VacationInfo>> GetEmployeeVacationsAsync(Person employee)
        {
            if (employee == null) throw new ArgumentNullException(nameof(employee));

            var allVacations = await _vacationRepository.GetAllAsync();
            var personVacations = allVacations.Where(v => v.EmployeeId == employee.Id);

            return personVacations;
        }
    }
}