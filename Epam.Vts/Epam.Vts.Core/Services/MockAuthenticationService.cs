﻿using System;
using System.Threading.Tasks;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.Core.Services
{
    public class MockAuthenticationService : IAuthenticationService
    {
        public Task<Person> AuthenticateAsync(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException(nameof(userName));
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException(nameof(password));

            // Just mock for now
            return Task.FromResult(new Person());
        }
    }
}