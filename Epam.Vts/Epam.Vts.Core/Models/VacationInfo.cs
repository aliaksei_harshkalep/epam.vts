﻿using System;

namespace Epam.Vts.Core.Models
{
    public class VacationInfo
    {
        public Person Approver { get; set; }

        public bool ConfirmationDocumentAvailable { get; set; }

        public TimeSpan Duration { get; set; }

        public string DurationStr { get; set; }

        public Person Employee { get; set; }

        public DateTime EndDate { get; set; }

        public int Id { get; set; }

        public string ProcessInstanceId { get; set; }

        public DateTime StartDate { get; set; }

        public IconedValue Status { get; set; }

        public IconedValue Type { get; set; }

        public object VacationForm { get; set; }
    }
}