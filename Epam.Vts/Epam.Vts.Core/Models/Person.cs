﻿namespace Epam.Vts.Core.Models
{
    public class Person
    {
        public string Email { get; set; }

        public string Id { get; set; }

        public string FullName { get; set; }

        public string Region { get; set; }
    }
}