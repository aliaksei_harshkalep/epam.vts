﻿namespace Epam.Vts.Core.Models
{
    public class IconedValue
    {
        public string Icon { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}