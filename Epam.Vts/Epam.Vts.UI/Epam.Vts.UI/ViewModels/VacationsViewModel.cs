﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Cirrious.MvvmCross.ViewModels;
using Epam.Vts.Core;
using Epam.Vts.Core.Repositories;
using Epam.Vts.Core.Services;
using Epam.Xmp.Vts.Entities;

namespace Epam.Vts.UI.ViewModels
{
    public class VacationsViewModel : MvxViewModel
    {
        private readonly IVacationService _vacationService;
        private readonly IPersonRepository _personRepository;
        private readonly IVacationStatusRepository _vacationStatusRepository;
        private readonly IVacationTypeRepository _vacationTypeRepository;
        private string _currentEmployeeId;

        private ObservableCollection<ShortVacationViewModel> _vacations;
        public ObservableCollection<ShortVacationViewModel> Vacations
        {
            get { return _vacations; }
            set
            {
                _vacations = value;
                RaisePropertyChanged();
            }
        }

        private string _employeeName;

        public string EmployeeName
        {
            get { return _employeeName; }
            private set { _employeeName = value; RaisePropertyChanged(); }
        }


        public VacationsViewModel(IVacationService vacationService, IPersonRepository personRepository, 
            IVacationStatusRepository vacationStatusRepository, IVacationTypeRepository vacationTypeRepository)
        {
            _vacationService = vacationService;
            _personRepository = personRepository;
            _vacationStatusRepository = vacationStatusRepository;
            _vacationTypeRepository = vacationTypeRepository;
        }

        public void Init(string currentEmployeeId)
        {
            _currentEmployeeId = currentEmployeeId;
        }

        public override async void Start()
        {
            var currentEmployee = await _personRepository.GetByIdAsync(_currentEmployeeId);
            var employeeVacs = await _vacationService.GetEmployeeVacationsAsync(currentEmployee);

            EmployeeName = currentEmployee.FullName;
            var vacationViewModels = await Task.WhenAll(employeeVacs.Select(async v => new ShortVacationViewModel
            {
                Id = v.Id,
                Duration = TimeSpan.FromMilliseconds(v.Duration).TotalHours / 8d,
                StartDate = DateTime.FromBinary(v.StartDate),
                EndDate = DateTime.FromBinary(v.EndDate),
                Type = (await _vacationTypeRepository.GetByIdAsync(v.TypeId)).Value,
                Status = (await _vacationStatusRepository.GetByIdAsync(v.StatusId)).Value,
                ApproverName = (await _personRepository.GetByIdAsync(v.ApproverId)).FullName
            }));

            Vacations = new ObservableCollection<ShortVacationViewModel>(vacationViewModels);

            base.Start();
        }
    }
}