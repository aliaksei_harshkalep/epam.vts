using System;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Epam.Vts.Core.Services;
using Epam.Vts.UI.Helpers;

namespace Epam.Vts.UI.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        private readonly IAuthenticationService _authenticationService;

        private string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            private set { _errorMessage = value; RaisePropertyChanged(); }
        }


        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            private set { _isBusy = value; RaisePropertyChanged(); }
        }


        private bool _isAuthenticated;
        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
            private set
            {
                _isAuthenticated = value;
                RaisePropertyChanged();
            }
        }

        private string _login;
        public string Login
        { 
            get { return _login; }
            set
            {
                _login = value;
                RaisePropertyChanged();
                RaisePropertyChanged(() => LoginCommand);
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                RaisePropertyChanged();
                RaisePropertyChanged(() => LoginCommand);
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                return new MvxCommand(
                    TryLogin, 
                    () => !string.IsNullOrEmpty(_login) && !string.IsNullOrEmpty(_password));
            }
        }

        public LoginViewModel(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        private async void TryLogin()
        {
            IsBusy = true;
            ErrorMessage = string.Empty;

            try
            {
                var person = await _authenticationService.AuthenticateAsync(_login, _password);
                if (person != null)
                {
                    IsAuthenticated = true;

                    var presentationBundle = NavigationHelper.CreateNavigationParamsBundle(NavigationHelper.NavigationMode.ClearStack);
                    ShowViewModel<VacationsViewModel>(new { currentEmployeeId = person.Id}, presentationBundle);
                }
                else
                {
                    ErrorMessage = "User name or password is invalid.";
                }
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
