﻿using System;

namespace Epam.Vts.UI.ViewModels
{
    public class ShortVacationViewModel
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Duration { get; set; }
        public string Status { get; set; }
        public string ApproverName { get; set; }
        public string Type { get; set; }
    }
}