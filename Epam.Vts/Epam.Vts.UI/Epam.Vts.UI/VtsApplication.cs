using System;
using Cirrious.CrossCore;
using Cirrious.CrossCore.IoC;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.ViewModels;
using Epam.Vts.Core;
using Epam.Vts.Core.Repositories;
using Epam.Vts.Core.Repositories.FileBased;
using Epam.Vts.Core.Repositories.ServiceBased;
using Epam.Vts.Core.Repositories.SqliteBased;
using Epam.Vts.Core.Services;
using Epam.Vts.UI.ViewModels;

namespace Epam.Vts.UI
{
    public class VtsApplication : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("ViewModel")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.RegisterType<IAuthenticationService, AuthenticationService>();
            Mvx.RegisterType<IVacationService, VacationService>();

#if SERVICE
            Mvx.RegisterType<IVacationRepository, ServiceVacationRepository>();
            Mvx.RegisterType<IPersonRepository, ServicePersonRepository>();
            Mvx.RegisterType<IVacationStatusRepository, ServiceVacationStatusRepository>();
            Mvx.RegisterType<IVacationTypeRepository, ServiceVacationTypeRepository>();
#elif FILE
            Mvx.RegisterType<IVacationRepository, FileVacationRepository>();
            Mvx.RegisterType<IPersonRepository, FilePersonRepository>();
            Mvx.RegisterType<IVacationStatusRepository, FileVacationStatusRepository>();
            Mvx.RegisterType<IVacationTypeRepository, FileVacationTypeRepository>();
#elif DB
            Mvx.RegisterType<IVacationRepository, SqliteVacationRepository>();
            Mvx.RegisterType<IPersonRepository, SqlitePersonRepository>();
            Mvx.RegisterType<IVacationStatusRepository, SqliteVacationStatusRepository>();
            Mvx.RegisterType<IVacationTypeRepository, SqliteVacationTypeRepository>();
#endif

            RegisterAppStart<LoginViewModel>();
        }
    }
}