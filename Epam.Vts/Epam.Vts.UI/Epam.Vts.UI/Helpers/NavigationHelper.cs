﻿using System;
using System.Collections.Generic;
using Cirrious.MvvmCross.ViewModels;

namespace Epam.Vts.UI.Helpers
{
    public static class NavigationHelper
    {
        private const string NavigationModeKey = "NavigationMode";

        public static MvxBundle CreateNavigationParamsBundle(NavigationMode navigationMode)
        {
            return new MvxBundle(CreateNavigationParams(navigationMode));
        }

        public static Dictionary<string, string> CreateNavigationParams(NavigationMode navigationMode)
        {
            return new Dictionary<string, string>
            {
                {
                    NavigationModeKey, navigationMode.ToString()
                }
            };
        }

        public static NavigationMode GetNavigationMode(IDictionary<string, string> dictionary)
        {
            if (dictionary == null || !dictionary.ContainsKey(NavigationModeKey))
            {
                return NavigationMode.Default;
            }

            NavigationMode mode;

            Enum.TryParse(dictionary[NavigationModeKey], out mode);

            return mode;
        }

        public enum NavigationMode
        {
            Default,
            ClearStack
        }
    }

}