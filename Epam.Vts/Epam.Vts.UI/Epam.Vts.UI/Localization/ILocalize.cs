﻿using System.Globalization;

namespace Epam.Vts.UI.Localization
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();

    }
}