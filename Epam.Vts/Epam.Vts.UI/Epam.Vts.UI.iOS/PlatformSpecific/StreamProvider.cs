﻿using System;
using System.IO;
using Epam.Vts.Core.Repositories.FileBased;

namespace Epam.Vts.UI.iOS.PlatformSpecific
{
    public class StreamProvider : IStreamProvider
    {
        public StreamReader GetReader(string path) => new StreamReader(GetFullPath(path));

        public StreamWriter GetWriter(string path) => new StreamWriter(GetFullPath(path));

        private static string GetFullPath(string path) => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), path);
    }
}