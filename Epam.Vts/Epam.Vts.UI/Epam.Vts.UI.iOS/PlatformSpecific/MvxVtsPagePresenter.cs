﻿using Cirrious.MvvmCross.Forms.Presenter.Touch;
using Cirrious.MvvmCross.ViewModels;
using Epam.Vts.UI.Helpers;
using UIKit;

namespace Epam.Vts.UI.iOS.PlatformSpecific
{
    public class MvxVtsPagePresenter : MvxFormsTouchPagePresenter
    {
        public MvxVtsPagePresenter(UIWindow window, Xamarin.Forms.Application mvxFormsApp) : base(window, mvxFormsApp)
        {
        }

        public override void Show(MvxViewModelRequest request)
        {
            base.Show(request);

            var navigationMode = NavigationHelper.GetNavigationMode(request.PresentationValues);
            if (navigationMode == NavigationHelper.NavigationMode.ClearStack)
            {
                // remove all pages except last
                while (MvxFormsApp.MainPage.Navigation.NavigationStack.Count > 1)
                {
                    MvxFormsApp.MainPage.Navigation.RemovePage(MvxFormsApp.MainPage.Navigation.NavigationStack[0]);
                }
            }
        }
    }
}