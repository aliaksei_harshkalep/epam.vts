﻿using System;
using Android.Content;
using Android.Graphics;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Droid.Platform;
using Cirrious.MvvmCross.Droid.Views;
using Cirrious.MvvmCross.Forms.Presenter.Core;
using Cirrious.MvvmCross.Forms.Presenter.Droid;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Views;
using Epam.Vts.Core.Repositories.FileBased;
using Epam.Vts.Core.Repositories.ServiceBased;
using Epam.Vts.Core.Services;
using Epam.Vts.UI;
using Epam.Vts.UI.Droid.PlatformSpecific;
using Epam.Vts.UI.Localization;
using Epam.Xmp.Vts.Entities;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Interop;
using SQLite.Net.Platform.XamarinAndroid;
using Path = System.IO.Path;

namespace Epam.Vts.UI.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new VtsApplication();
        }

        protected override void InitializeIoC()
        {
            base.InitializeIoC();

            Mvx.RegisterType<ILocalize, Localize>();

#if INITFILES
            InitializeFiles();
#endif

            Mvx.RegisterType<IStreamProvider, StreamProvider>();

            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "vacs.db");

#if INITDB
            InitializeDatabase(path);
#endif

            Mvx.RegisterType(() => new SQLiteAsyncConnection(() =>
                new SQLiteConnectionWithLock(new SQLitePlatformAndroid(), new SQLiteConnectionString(path, true))));
        }

        private static void InitializeDatabase(string path)
        {
            var conn = new SQLiteConnection(new SQLitePlatformAndroid(), path);

            var authService = new AuthenticationService();
            authService.Authenticate("vasil_pupkin@epam.com", "secret");

            var typesRepository = new ServiceVacationTypeRepository();
            conn.CreateTable<VacationType>();
            conn.InsertOrIgnoreAll(typesRepository.GetAll());

            var statusesRepository = new ServiceVacationStatusRepository();
            conn.CreateTable<VacationStatus>();
            conn.InsertOrIgnoreAll(statusesRepository.GetAll());

            var personsRepository = new ServicePersonRepository();
            conn.CreateTable<Person>();
            conn.InsertOrIgnoreAll(personsRepository.GetAll());

            var vacationsRepository = new ServiceVacationRepository();
            conn.CreateTable<VacationInfo>();
            conn.InsertOrIgnoreAll(vacationsRepository.GetAll());
        }

        private static void InitializeFiles()
        {
            var authService = new AuthenticationService();
            authService.Authenticate("vasil_pupkin@epam.com", "secret");

            var typesRepository = new ServiceVacationTypeRepository();
            var fileTypesRepository = new FileVacationTypeRepository(new StreamProvider());
            fileTypesRepository.AddOrUpdate(typesRepository.GetAll());

            var statusesRepository = new ServiceVacationStatusRepository();
            var fileStatusesRepository = new FileVacationStatusRepository(new StreamProvider());
            fileStatusesRepository.AddOrUpdate(statusesRepository.GetAll());

            var personsRepository = new ServicePersonRepository();
            var filePersonsRepository = new FilePersonRepository(new StreamProvider());
            filePersonsRepository.AddOrUpdate(personsRepository.GetAll());

            var vacationsRepository = new ServiceVacationRepository();
            var fileVacationsRepository = new FileVacationRepository(new StreamProvider());
            fileVacationsRepository.AddOrUpdate(vacationsRepository.GetAll());
        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter()
        {
            var presenter = new MvxVtsPagePresenter {MvxFormsApp = new MvxFormsApp()};

            Mvx.RegisterSingleton<IMvxViewPresenter>(presenter);

            return presenter;
        }
    }
}