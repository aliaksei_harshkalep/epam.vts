﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;
using Xamarin.Forms;

namespace Epam.Vts.UI.Droid.Activities
{
    [Activity(Label = "Epam VTS tracking", Icon = "@drawable/favicon", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, NoHistory = true)]
    public class MainActivity : MvxSplashScreenActivity
    {
        public override void InitializationComplete()
        {
            StartActivity(typeof(MvxFormsApplicationMainActivity));
        }

        protected override void OnCreate(Bundle bundle)
        {
            Forms.Init(this, bundle);

            // Leverage controls' StyleId attrib. to Xamarin.UITest
            Forms.ViewInitialized += (sender, e) =>
            {
                if (!string.IsNullOrWhiteSpace(e.View.StyleId))
                {
                    e.NativeView.ContentDescription = e.View.StyleId;
                }
            };

            base.OnCreate(bundle);
        }
    }
}