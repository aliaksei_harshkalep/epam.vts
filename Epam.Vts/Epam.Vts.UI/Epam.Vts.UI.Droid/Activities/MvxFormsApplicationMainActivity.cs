using Android.App;
using Android.OS;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Forms.Presenter.Droid;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace Epam.Vts.UI.Droid.Activities
{
    [Activity(Label = "Epam VTS tracking", Icon = "@drawable/favicon")]
    public class MvxFormsApplicationMainActivity : FormsApplicationActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Forms.Init(this, savedInstanceState);

            var presenter = Mvx.Resolve<IMvxViewPresenter>() as MvxFormsDroidPagePresenter;
            if (presenter == null) return;

            LoadApplication(presenter.MvxFormsApp);

            Mvx.Resolve<IMvxAppStart>().Start();
        }

    }
}