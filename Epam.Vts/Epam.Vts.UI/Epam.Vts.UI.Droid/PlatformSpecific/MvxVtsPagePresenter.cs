﻿using Cirrious.MvvmCross.Forms.Presenter.Droid;
using Cirrious.MvvmCross.ViewModels;
using Epam.Vts.UI.Helpers;

namespace Epam.Vts.UI.Droid.PlatformSpecific
{
    public class MvxVtsPagePresenter : MvxFormsDroidPagePresenter
    {
        public override void Show(MvxViewModelRequest request)
        {
            base.Show(request);

            var navigationMode = NavigationHelper.GetNavigationMode(request.PresentationValues);
            if (navigationMode == NavigationHelper.NavigationMode.ClearStack)
            {
                // remove all pages except last
                while (MvxFormsApp.MainPage.Navigation.NavigationStack.Count > 1)
                {
                    MvxFormsApp.MainPage.Navigation.RemovePage(MvxFormsApp.MainPage.Navigation.NavigationStack[0]);
                }
            }
        }
    }
}